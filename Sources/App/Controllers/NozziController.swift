import Vapor
import SwiftSoup
import Foundation
#if canImport(FoundationNetworking)
import FoundationNetworking
#endif

final class NozziController {
    enum Error: Swift.Error {
        enum URL: Swift.Error {
            case extractID
            case printURL
        }
        enum Parsing: Swift.Error {
            case noResponseData
            case cantConvertToUTF8
        }
        enum Fetching: Swift.Error {
            case noData
        }
    }
    
    private let worker: Worker
    
    init(worker: Worker) {
        self.worker = worker
    }
    
    func parse(url: String) throws -> Future<Article> {
        let contentID = try extractContentID(url)
        let printURL = try makePrintURL(id: contentID)
        return fetchData(printURL)
            .map { try self.parseData($0) }
    }
    
    private func parseData(_ data: Data) throws -> Article {
        guard let htmlString = String(data: data, encoding: .utf8) else {
            throw Error.Parsing.cantConvertToUTF8
        }
        
        let document = try SwiftSoup.parse(htmlString)
        let title = try document
            .getElementsByTag("h2")
            .first()?
            .text() ?? "Title not Found"
        
        let content = try document
            .getElementById("content")
        
        let contentTexts = try content?
            .getAllElements()
            .array()
            .filter { ["p", "li"].contains($0.tag().getName()) }
            .map { element -> Article.Paragraph in
                let text = try element.text()
                switch element.tag().getName() {
                case "li":
                    return Article.Paragraph.listItem(text)
                default:
                    return Article.Paragraph.text(text)
                }
            }
        
        let subtitle = try content?
            .previousElementSibling()?
            .text() ?? "Subtitle not Found"
        
        return Article(title: title, subtitle: subtitle, paragraphs: contentTexts ?? [])
    }
    
    private func fetchData(_ url: URL) -> Future<Data> {
        let promise = worker.eventLoop.newPromise(Data.self)
        
        URLSession.shared.dataTask(with: url) { (data, response, e) in
            if let error = e {
                promise.fail(error: error)
                return
            }
            
            guard let data = data else {
                promise.fail(error: Error.Fetching.noData)
                return
            }
            
            promise.succeed(result: data)
        }.resume()
        
        return promise.futureResult
    }
    
    private func extractContentID(_ url: String) throws -> Int {
        let contentID = url
            .split(separator: "/")
            .compactMap { Int($0) }
            .first
        
        if contentID == nil {
            throw Error.URL.extractID
        }
        
        return contentID!
    }
    
    private func makePrintURL(id: Int) throws -> URL {
        guard let printURL = URL(string: "https://www.noz.de/socialmediabar/print/article/\(id)") else {
            throw Error.URL.printURL
        }
        return printURL
    }
}
