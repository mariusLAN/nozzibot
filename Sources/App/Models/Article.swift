import Vapor
struct Article {
    enum Paragraph {
        case text(String)
        case listItem(String)
        
        var textRepresentation: String {
            switch self {
            case .listItem(let text):
                return "• \(text)\n"
            case .text(let text):
                return text + "\n\n"
            }
        }
    }
    let title: String
    let subtitle: String
    let paragraphs: [Paragraph]
}

extension Article {
    var messages: [String] {
        var texts: [String] = []
        var text: String = ""
        for paragraph in paragraphs {
            if text.count + paragraph.textRepresentation.count > 4000 {
                texts.append(text)
                text = ""
            }
            text = text + paragraph.textRepresentation
        }
        if !text.isEmpty {
            texts.append(text)
        }
        return texts
    }
}
