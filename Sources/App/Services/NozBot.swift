import Foundation
import Telegrammer
import Vapor

final class NozBot: ServiceType {
    
    let bot: Bot
    var updater: Updater?
    var dispatcher: Dispatcher?
    var nozziController: NozziController?
    
    ///Conformance to `ServiceType` protocol, fabric methhod
    static func makeService(for worker: Container) throws -> NozBot {
        let token = "269305852:AAFJ6lpH1K9Ypy5xZySHQZVcezvE37Apyz4"
        let settings = Bot.Settings(token: token, debugMode: true)
        return try NozBot(settings: settings)
    }
    
    init(settings: Bot.Settings) throws {
        self.bot = try Bot(settings: settings)
        let dispatcher = try configureDispatcher()
        self.dispatcher = dispatcher
        self.updater = Updater(bot: bot, dispatcher: dispatcher)
        self.nozziController = NozziController(worker: dispatcher.worker)
    }
    
    /// Initializing dispatcher, object that receive updates from Updater
    /// and pass them throught handlers pipeline
    func configureDispatcher() throws -> Dispatcher {
        ///Dispatcher - handle all incoming messages
        let dispatcher = Dispatcher(bot: bot)
        
        ///Creating and adding handler for ordinary text messages
        let nozMessageHandler = MessageHandler(filters: Filters.text, callback: nozResponse)
        dispatcher.add(handler: nozMessageHandler)
        
        return dispatcher
    }
}

extension NozBot {
    func nozResponse(_ update: Update, _ context: BotContext?) throws {
        guard let message = update.message else {
            return
        }
        
        let articleFuture  = try nozziController?.parse(url: message.text!)
        articleFuture?.whenSuccess { article in
            article.messages.forEach {
                _ = try? self.bot.sendMessage(params: .init(chatId: .chat(message.chat.id),
                                                            text: $0))
            }
        }
        
        articleFuture?.whenFailure({ error in
            let errorMessage = "Error: \(String(describing: error))"
            _ = try? self.bot.sendMessage(params: .init(chatId: .chat(message.chat.id),
                                                        text: errorMessage))
        })
    }
}
