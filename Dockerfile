FROM swift:latest

WORKDIR /app

ADD . ./

# Dependencies
RUN apt-get -q update
RUN apt-get install -y libssl-dev libicu-dev

# Swift
RUN swift package clean
RUN swift build -c release
RUN mkdir /app/bin
RUN mv `swift build -c release --show-bin-path` /app/bin

# Configuration
EXPOSE 8080

ENTRYPOINT ./bin/release/App serve -e prod -b 0.0.0.0
